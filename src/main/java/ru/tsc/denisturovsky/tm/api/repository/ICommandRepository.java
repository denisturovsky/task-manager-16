package ru.tsc.denisturovsky.tm.api.repository;

import ru.tsc.denisturovsky.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
