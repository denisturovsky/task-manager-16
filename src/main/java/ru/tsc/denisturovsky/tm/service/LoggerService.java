package ru.tsc.denisturovsky.tm.service;

import ru.tsc.denisturovsky.tm.api.service.ILoggerService;

import java.io.IOException;
import java.util.logging.*;

public class LoggerService implements ILoggerService {

    private final static String CONFIG_FILE = "/logger.properties";

    private final static String COMMANDS = "COMMANDS";

    private final static String COMMANDS_FILE = "./commands.xml";

    private final static String ERRORS = "ERRORS";

    private final static String ERRORS_FILE = "./errors.xml";

    private final static String MESSAGES = "MESSAGES";

    private final static String MESSAGE_FILE = "./messages.xml";

    private final LogManager manager = LogManager.getLogManager();

    private final Logger root = Logger.getLogger("");

    private final Logger commands = Logger.getLogger(COMMANDS);

    private final Logger errors = Logger.getLogger(ERRORS);

    private final Logger messages = Logger.getLogger(MESSAGES);

    private final ConsoleHandler consoleHandler = getConsoleHandler();

    {
        init();
        registry(commands, COMMANDS_FILE, false);
        registry(errors, ERRORS_FILE, true);
        registry(messages, MESSAGE_FILE, true);
    }

    private void init() {
        try {
            manager.readConfiguration(LoggerService.class.getResourceAsStream(CONFIG_FILE));
        } catch (final IOException e) {
            root.severe(e.getMessage());
        }
    }

    private ConsoleHandler getConsoleHandler() {
        final ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(new Formatter() {
            public String format(LogRecord record) {
                return record.getMessage() + "\n";
            }
        });
        return handler;
    }

    private void registry(final Logger logger, final String fileName, final boolean isConsole) {
        try {
            if (isConsole) logger.addHandler(consoleHandler);
            logger.setUseParentHandlers(false);
            logger.addHandler(new FileHandler(fileName));
        } catch (final IOException e) {
            root.severe(e.getMessage());
        }
    }

    @Override
    public void info(final String message) {
        if (message == null || message.isEmpty()) return;
        messages.info(message);
    }

    @Override
    public void debug(final String message) {
        if (message == null || message.isEmpty()) return;
        messages.fine(message);
    }

    @Override
    public void command(final String message) {
        if (message == null || message.isEmpty()) return;
        commands.info(message);
    }

    @Override
    public void error(final Exception e) {
        if (e == null) return;
        errors.log(Level.SEVERE, e.getMessage(), e);
    }

}
